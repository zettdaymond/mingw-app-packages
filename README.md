# MINGW-app-packages #
This is small *unofficial* MSYS2 repository that provides PKGBUILDs of some useful MinGW-w64 (native windows) **applications**.
A PKGBUILD is a shell script containing the build information required by Arch Linux and MSYS2 packages -- from [https://wiki.archlinux.org/index.php/PKGBUILD](https://wiki.archlinux.org/index.php/PKGBUILD). 

To build a package, run msys2_shell.bat and enter the following commands in the bash prompt:
```
$ cd /cloned/repo/dir/some_package/
$ makepkg-mingw
```

After the package has been build, install it, e.g.:
```
$ pacman -U mingw-w64-x86_64-some_package.pkg.tar.xz
```